package com.ochkin.calculator;

public class Calculator {

    private Double val1;
    private Double val2;
    private int action;

    public Double calculate() {
        System.out.println("val1 " + val1);
        System.out.println("val2 " + val2);
        System.out.println("action " + action);
        Double result = 0.00;
        switch (action) {
            case 0:
                result = val1 + val2;
                break;
            case 1:
                result = val1 - val2;
                break;
            case 2:
                result = val1 / val2;
                break;
            case 3:
                result = val1 * val2;
                break;
        }
        return result;
    }

    public Double getVal1() {
        return val1;
    }

    public void setVal1(Double val1) {
        this.val1 = val1;
    }

    public Double getVal2() {
        return val2;
    }

    public void setVal2(Double val2) {
        this.val2 = val2;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}

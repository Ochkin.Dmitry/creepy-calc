package com.ochkin.calculator;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

import java.util.HashMap;
import java.util.Map;


public class MainController {

    private final static int ACTION_SUMM = 0;
    private final static int ACTION_MIN = 1;
    private final static int ACTION_DIVIDE = 2;
    private final static int ACTION_MULTIPLY = 3;
    private final static int ACTION_RESULT = 4;
    private final static int ACTION_RESET = 5;

    private TextField valuesField;
    private TextField resultField;
    private Button[] numbers;
    private Button resetButton;
    private Map<Integer, Button> actions;
    private GridPane buttonsArea;
    private Calculator calc;
    private boolean isVal1;

    public MainController() {
        valuesField = new TextField();
        resultField = new TextField();
        numbers = new Button[10];
        resetButton = new Button();
        actions = new HashMap<>();
        buttonsArea = new GridPane();
        calc = new Calculator();
    }

    public Scene initView() {
        AnchorPane root = new AnchorPane();
        HBox topArea = new HBox();
        VBox cButton = new VBox();
        topArea.setPrefHeight(70);
        resultField.setDisable(true);
        valuesField.setPrefWidth(90);
        resultField.setPrefWidth(150);

        AnchorPane.setTopAnchor(topArea, 5.0);
        AnchorPane.setRightAnchor(topArea, 5.0);
        AnchorPane.setLeftAnchor(topArea, 5.0);
        AnchorPane.setRightAnchor(cButton, 5.0);
        AnchorPane.setLeftAnchor(cButton, 5.0);
        AnchorPane.setTopAnchor(cButton, 75.0);
        AnchorPane.setTopAnchor(buttonsArea, 100.0);
        AnchorPane.setRightAnchor(buttonsArea, 5.0);
        AnchorPane.setLeftAnchor(buttonsArea, 5.0);
        AnchorPane.setBottomAnchor(buttonsArea, 5.0);

        root.getChildren().addAll(topArea, cButton, buttonsArea);
        topArea.getChildren().addAll(valuesField, resultField);
        topArea.setSpacing(20.0);

        cButton.getChildren().add(resetButton);
        createButtons();
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPrefHeight(40);
        ColumnConstraints col = new ColumnConstraints();
        ColumnConstraints col2 = new ColumnConstraints();
        col.setPercentWidth(30);
        col2.setPercentWidth(40);
        buttonsArea.getColumnConstraints().add(0, col);
        buttonsArea.getColumnConstraints().add(1, col);
        buttonsArea.getColumnConstraints().add(2, col2);
        buttonsArea.getRowConstraints().add(0, rowConstraints);
        buttonsArea.getRowConstraints().add(1, rowConstraints);
        buttonsArea.getRowConstraints().add(2, rowConstraints);
        buttonsArea.getRowConstraints().add(3, rowConstraints);
        buttonsArea.getRowConstraints().add(4, rowConstraints);
        for (Button number : numbers) {
            number.setPrefWidth(50);
        }
        actions.forEach((integer, button) -> button.setPrefWidth(65));
        return new Scene(root, 300.0, 250.0);
    }

    private void createButtons() {
        for (int i = 0; i < 10; i++) {
            numbers[i] = new Button();
            numbers[i].setText(String.valueOf(i));
        }
        int i = 1;
        buttonsArea.add(numbers[i++], 0, 0);
        buttonsArea.add(numbers[i++], 1, 0);
        buttonsArea.add(numbers[i++], 0, 1);
        buttonsArea.add(numbers[i++], 1, 1);
        buttonsArea.add(numbers[i++], 0, 2);
        buttonsArea.add(numbers[i++], 1, 2);
        buttonsArea.add(numbers[i++], 0, 3);
        buttonsArea.add(numbers[i++], 1, 3);
        buttonsArea.add(numbers[i], 0, 4);
        buttonsArea.add(numbers[0], 1, 4);

        actions.put(ACTION_SUMM, new Button());
        actions.get(ACTION_SUMM).setText("+");
        actions.put(ACTION_MIN, new Button());
        actions.get(ACTION_MIN).setText("-");
        actions.put(ACTION_DIVIDE, new Button());
        actions.get(ACTION_DIVIDE).setText("/");
        actions.put(ACTION_MULTIPLY, new Button());
        actions.get(ACTION_MULTIPLY).setText("*");
        actions.put(ACTION_RESULT, new Button());
        actions.get(ACTION_RESULT).setText("=");
        actions.put(ACTION_RESET, resetButton);
        actions.get(ACTION_RESET).setText("C");

        buttonsArea.add(actions.get(ACTION_SUMM), 2, 0);
        buttonsArea.add(actions.get(ACTION_MIN), 2, 1);
        buttonsArea.add(actions.get(ACTION_DIVIDE), 2, 2);
        buttonsArea.add(actions.get(ACTION_MULTIPLY), 2, 3);
        buttonsArea.add(actions.get(ACTION_RESULT), 2, 4);
        for (Button number : numbers) {
            number.setOnMouseClicked((event -> {
                if (valuesField.getText() != null) {
                    String currentText = (valuesField.getText() + number.getText());
                    valuesField.setText(currentText);
                    if (!isVal1) {
                        calc.setVal1(Double.valueOf(currentText));
                        System.out.println("Otdaem v val1:  " + Double.valueOf(currentText));
                    } else {
                        calc.setVal2(Double.valueOf(currentText));
                        System.out.println("Otdaem v val2:  " + Double.valueOf(currentText));
                    }
                }
            }));
        }
        actions.forEach((integer, button) -> button.setOnMouseClicked(event -> {
            if (calc.getVal1() != null) {
                if (integer != ACTION_RESULT & integer != ACTION_RESET) {
                    calc.setAction(integer);
                    isVal1 = true;
                } else if (integer == ACTION_RESET) {
                    isVal1 = false;
                    resultField.clear();
                } else {
                    if (calc.getVal2() != null) {
                        if (calc.getVal2() == 0 & calc.getAction() == ACTION_DIVIDE) {
                            resultField.setText("НИЗЯ ДИЛИТЬ НА НОЛЬ");
                        } else {
                            double result = calc.calculate();
                            if ((result == Math.floor(result)) && !Double.isInfinite(result)) {
                                int resInt = (int) result;
                                resultField.setText(String.valueOf(resInt));
                            } else {
                                resultField.setText(String.valueOf(result));
                            }
                            isVal1 = false;
                        }
                    }
                }
            }
            valuesField.clear();
        }));
    }
}

